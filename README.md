# Boostnote Appimage build

[boostnote](https://github.com/BoostIO/Boostnote) is a markdown editor - agnostic binary software packaging.

Download **Boostnote AppImage** binary from here: [Boostnote.AppImage](https://gitlab.com/linuxappimage/boostnote-appimage/-/jobs/artifacts/master/raw/Boostnote.AppImage?job=run-build)

Notes for you Linux shell:

```sh
curl -sLo Boostnote.AppImage https://gitlab.com/linuxappimage/boostnote-appimage/-/jobs/artifacts/master/raw/Boostnote.AppImage?job=run-build
chmod +x Boostnote.AppImage
./Boostnote.AppImage
```
